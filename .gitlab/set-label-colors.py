#!/usr/bin/env python3

import os
import gitlab
import sys
import yaml
from colorama import Fore, Style


COLORS = {
    # bright
    'actions': '#0bdf08',  # green
    'warnings': '#ffc040',  # orange
    'errors': '#ff0000',  # red
    # smooth
    'features': '#eed5ee',  # light pink
    'languages': '#e8d8cb',  # light brown
    'tools': '#bfe7f2',  # light blue
    'status': '#dbecd8',  # light green
    'group_funding': '#edf2d5',  # light yellow
}


def main():
    private_token = os.getenv('PERSONAL_ACCESS_TOKEN')
    if not private_token:
        print(
            Fore.RED
            + 'ERROR: GitLab Token not found in PERSONAL_ACCESS_TOKEN!'
            + Style.RESET_ALL
        )
        sys.exit(1)
    path = os.getenv('CI_PROJECT_PATH')
    if not path:
        print(
            Fore.RED
            + 'ERROR: project path not found CI_PROJECT_PATH!'
            + Style.RESET_ALL
        )
        sys.exit(1)
    gl = gitlab.Gitlab('https://gitlab.com', api_version=4, private_token=private_token)
    project = gl.projects.get(path, lazy=True)

    with open(os.path.join(os.path.dirname(__file__), 'label-categories.yaml')) as fp:
        mapping = yaml.safe_load(fp)
    labels_to_modify = set()
    for labels in mapping.values():
        labels_to_modify.update(labels)
    print('labels to modify:', sorted(labels_to_modify))
    print('label categories:', sorted(mapping.keys()))

    skipped = set()
    for page in range(12):
        for label in project.labels.list(page=page, per_page=100):
            if not label.is_project_label:
                continue
            if '.' not in label.name and label.name not in labels_to_modify:
                skipped.add(label.name)
                continue
            label.color = '#eee'
            for k, v in mapping.items():
                if label.name in v:
                    label.color = COLORS[k]
            print(label.name, label.color)
            label.save()
    print('skipped:', sorted(skipped))


if __name__ == "__main__":
    main()
